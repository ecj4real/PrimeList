﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace PrimeList
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch myWatch = new Stopwatch();
            
            Console.Write("Please enter the First Number: ");
            int first = Convert.ToInt32(Console.ReadLine());
        
            Console.Write("Please enter the Last Number: ");
            int last = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine();

            myWatch.Start();
            if (first > last)
            {
                Console.WriteLine("Sorry, Last Number should be greater than First Number");
            }
            else
            {
                List<int> primes = new List<int>();

                for (int i = first; i <= last; i++)
                {
                    if (isPrime(i))
                    {
                        primes.Add(i);                      
                    }
                }
                if (primes.Count == 0)
                {
                    Console.WriteLine("No prime number exists betweeen the numbers");
                }
                else
                {
                    Console.WriteLine("The prime pumber(s) is/are: ");
                    for(int i=0; i<primes.Count; i++)
                    {
                        Console.Write(" ," + primes[i]);
                    }
                }
            }

            myWatch.Stop();
            Console.WriteLine();
            Console.WriteLine("time: {0}", myWatch.Elapsed);
            Console.ReadKey();
        }

        public static bool isPrime(int number)
        {
            if (number <= 1)
            {
                return false;
            }
            if (number == 2)
            {
                return true;
            }

            for (int i = 2; i * i <= number; i++)
            {
                if (number % i == 0)
                {
                    return false;
                }
            }
            return true;
        }
    }

   
}
